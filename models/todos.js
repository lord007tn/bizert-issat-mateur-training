 /**
We import our mongoose package here because it had some function which help us to make instruction to the MongoDB database
 **/
const mongoose = require("mongoose");
 /**
We declare our schema from mongoose.Schema
 **/
const Schema = mongoose.Schema;

 /**
This is the table of our To-do list where we give a name, description and a create_time

ps: maybe you could develop it and add some other variables like checked or not,
or you may add a login system where you assign To-do to a specific client and so on.
 **/
const TodoSchema = new Schema({
    name: {type: String, require:true, min:6, max: 255},
    description: {type: String, require:true},
    create_time: {type: Date, default: Date.now}
});
// here where we export our models and as you see we made this with mongoose.model() so it will have the default structure of the MongoDB.
module.exports = mongoose.model('todos',TodoSchema);

