/**
This where we import the Router() from our express package to use it and generate valid routers for our Web app.
**/
const router = require("express").Router();

/**
We just import the controllers ( CRUD ) from our controllers file.
so we can assign every function to a valid router.
 **/
const todosController = require("../controllers/todos.controller")

/**
 This are all the routers if you want to know more just take a look here
 https://expressjs.com/en/guide/routing.html
**/
router.post('/create', todosController.createTodo);
router.get('/todos',todosController.getTodos);
router.get('/todos/:id', todosController.getTodo);
router.put('/todos/:id/update', todosController.updateTodo);
router.delete('/todos/:id/delete', todosController.deleteTodo);
// we export our routers so we can import them in the index.js file.
module.exports = router;