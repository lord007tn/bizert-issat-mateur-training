/**
 This what we did in the last workshop about Nodejs, we just made our server run on the 8000 port.
 so as a challenge i add some other functions and i build the whole CRUD system for a TODO list app.
 I added comments to let you know and understand how things work. but before i need you to watch this videos first.

 - Nodejs in 1h
 https://www.youtube.com/watch?v=TlB_eWDSMt4

 - Full Nodejs basics
 https://www.youtube.com/watch?v=RLtyhwFtXQA

 - Nodejs with mongoose
 https://www.youtube.com/watch?v=4yqu8YF29cU
 **/


 /**
So this were we import our packages, express mongoose dotenv
 **/
const express = require("express");
const app = express();

const mongoose = require("mongoose");

/**
dotenv is a package that let you load from .env file you can check it here to know more.
https://www.npmjs.com/package/dotenv
**/
const dotenv = require("dotenv")
dotenv.config();
/**
This need to included to get rid of some deprecated . if you want to know more just read this.
https://mongoosejs.com/docs/deprecations.html
 **/
/**
You can know about these part and how you could make your own Database in the video that i linked above.
**/
 //Connect to Database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect( process.env.DB_CONNECT, () => console.log( "connected to db" ) );

/**
If you watch the videos above you will know that this called routers of your web app.
 **/
//Import routers
const todoRouter = require("./routes/todos.route");

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Routers Middleware
app.use('/api', todoRouter);


let port = 8000;

app.listen(port, ()=>{
    console.log(" this server is running on port "+ port);
});