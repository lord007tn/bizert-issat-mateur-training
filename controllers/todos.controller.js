/**
So this the file of Controllers where we made our CRUD every function do a certain HTTP method request, Create Read Update Delete.
 **/

 /**
We import our Models in the Todos.js that represent our data base structure and types ... (as simple explanation for now)
 **/
const Todos = require("../models/todos")
 /**
We import our mongoose package here because it had some function which help us to make instruction to the MongoDB database
 **/
const mongoose = require("mongoose")

// function to create a To-Do list
const createTodo = async(req, res)=>{

    const todo = new Todos({
        name: req.body.name,
        description: req.body.description
    })
    try{
        const savedTodo = await todo.save();
        res.send({user: todo._id});
    }catch(err){
        throw res.status(400).send(err)
    }
}

// function to get all the To-Dos list that we have in data base 
const getTodos = async(req, res) => {
    try {
        const todos = await Todos.find();
        return res.send(todos)
    } catch (err) {
        throw res.send(err)
    }
}

// function to get a single To-Do in our database by ID
const getTodo = async(req, res) => {
    const todoID = req.params.id;

    if(!mongoose.Types.ObjectId.isValid(todoID)){return res.send("not found")}
    else{
        const todo = await Todos.findById(todoID);
        return res.send(todo)
    }
}

// function to Update the To-Do element that you get by the ID!
const updateTodo = async(req, res)=>{

    try {
        const todoID = req.params.id;
        const todo = req.body;
        const {...updatedata} = todo;
        const updatetodo = await Todos.findByIdAndUpdate(todoID, updatedata, {new: true});
        return res.send(updateTodo)
    } catch (err) {
        throw res.send(err)
    }
}

// function to Delete a To-Do element by ID
const deleteTodo = async(req, res)=> {

    try {
        const todoID = req.params.id;
        const todo = await Todos.findByIdAndDelete(todoID)
        return res.send(todoID)
    } catch (err) {
        throw res.send(err)
    }
}

//this were we export our functions, so we can call them in other js file when we import it
module.exports.getTodo = getTodo;
module.exports.getTodos = getTodos;
module.exports.updateTodo = updateTodo;
module.exports.deleteTodo = deleteTodo;
module.exports.createTodo = createTodo;